import UIKit




class TableViewCell: UITableViewCell{
  
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var timerLabel: UILabel!
  @IBOutlet weak var isDoneLabel: UILabel!
  @IBOutlet weak var imageLabel: UIImageView!
  @IBOutlet weak var cellView: UIView!
  @IBOutlet weak var imageScrin: UIImageView!
 
  func configure(model: Any) {
    guard let model = model as? TableViewCellModel else { return }

    nameLabel.text = model.name
    descriptionLabel.text = model.descript
    imageLabel.image = model.image
    imageScrin.image = model.screenshot
    
    let strDate = Date()
    let timeInterval = -strDate.timeIntervalSince(MyDateformatter.dateFormatter.date(from: model.time)!)/60
    
    if model.isDone {
      isDoneLabel.text = "Done"
      cellView.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
    } else {
      isDoneLabel.text = "Not done"
      if timeInterval > 0 {
        cellView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
      } else {
        cellView.backgroundColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
      }
    }
    if timeInterval > 0 && timeInterval < 60 {
      
      timerLabel.text = "\(Int(timeInterval)) minutes left"
      if timeInterval < 1 {
        timerLabel.text = "1 minute"
      }
    } else {
      timerLabel.text = model.time
    }
  }
  
}
