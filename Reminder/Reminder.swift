//
//  Reminder.swift
//  Reminder
//
//  Created by Admin on 30.11.16.
//  Copyright © 2016 Mikalai. All rights reserved.
//

import UIKit

class Reminder: NSObject, NSCoding {
  var name: String
  var descript: String
  var isDone: Bool
  var time: Date
  var identity: Int
  var image: String
  var screenshot: String
  
  init(name: String, descript: String, isDone: Bool, time: Date, identity: Int, image: String, screenshot: String) {
    self.name = name
    self.descript = descript
    self.isDone = isDone
    self.time = time
    self.identity = identity
    self.image = image
    self.screenshot = screenshot
  }
  
  required convenience init(coder aDecoder: NSCoder) {
    let name = aDecoder.decodeObject(forKey: "name") as! String
    let descript = aDecoder.decodeObject(forKey: "descript") as! String
    let isDone = aDecoder.decodeBool(forKey: "isDone")
    let time = aDecoder.decodeObject(forKey: "time") as! Date
    let identity = aDecoder.decodeInt64(forKey: "identity")
    let image = aDecoder.decodeObject(forKey: "image") as! String
    let screenshot = aDecoder.decodeObject(forKey: "screenshot") as! String
    
    self.init(
      name: name,
      descript: descript,
      isDone: isDone,
      time: time,
      identity: Int(identity),
      image: image,
      screenshot: screenshot
    )
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(name, forKey: "name")
    aCoder.encode(descript, forKey: "descript")
    aCoder.encode(isDone, forKey: "isDone")
    aCoder.encode(time, forKey: "time")
    aCoder.encode(identity, forKey: "identity")
    aCoder.encode(image, forKey: "image")
    aCoder.encode(screenshot, forKey: "screenshot")
  }
  static func getReminders() -> [Reminder] {
    if let data = UserDefaults.standard.value(forKey: "reminders") as? Data, let reminders
      = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Reminder]{
      return reminders
    } else {
      return []
    }
  }
  
  static func setReminders(reminders: [Reminder]) {
    let arrayForSet = reminders.sorted(by: { $0.time.compare($1.time) == ComparisonResult.orderedAscending })
    for (numberRem, reminder) in arrayForSet.enumerated() {
      reminder.identity = numberRem
    }
    let encodeData = NSKeyedArchiver.archivedData(withRootObject: arrayForSet)
    UserDefaults.standard.set(encodeData, forKey: "reminders")
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Was added a new reminder"), object: nil)
  }
  
  static func getImageUrl(name: String) -> URL {
    
    let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    let docUrl = documentsUrl.appendingPathComponent("customDirectory")
    let docPath = documentsUrl.appendingPathComponent("customDirectory").path
    if !FileManager.default.fileExists(atPath: docPath) {
      try? FileManager.default.createDirectory(at: docUrl, withIntermediateDirectories: false, attributes: nil)
    }
    let imageUrl = docUrl.appendingPathComponent("\(name).png")
    return imageUrl
  }
  
  static func saveImage(name: String, image: UIImage) {
    try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: name).path)
    if let pngImageData = UIImagePNGRepresentation(image) {
      try? pngImageData.write(to: Reminder.getImageUrl(name: name), options: .atomic)
    } 
  }
  
  static func deleteImage(name: String) {
    try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: name).path)
  }
}
