//
//  TextFieldTableViewCell.swift
//  Reminder
//
//  Created by Admin on 27.04.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell, Configurable, UITextFieldDelegate {
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var nameTextField: UITextField!
  
  let datePicker = UIDatePicker()
  
  var action: ((String) -> ())!
  
  override func awakeFromNib() {
    super.awakeFromNib()

    nameTextField.delegate = self
  }
  
  func datePickerValueChanged () {
    nameTextField.text = MyDateformatter.dateFormatter.string(from: datePicker.date)
  }
  
  func configureCellForDatePicker () {
    datePicker.addTarget(self, action: #selector(TextFieldTableViewCell.datePickerValueChanged), for: UIControlEvents.valueChanged)
    nameTextField.inputView = datePicker
  }
  
  func configure (model: Any) {
    let model = model as! TextFieldCellModel
    if model.label == "Time" {
      datePicker.addTarget(self, action: #selector(TextFieldTableViewCell.datePickerValueChanged), for: UIControlEvents.valueChanged)
      nameTextField.inputView = datePicker
    } else {
      nameTextField.text = model.text
    }
    nameTextField.tag = model.tag
    nameLabel.text = model.label
    nameTextField.placeholder = model.label
    action = model.action
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    self.action(textField.text!)
}

}
