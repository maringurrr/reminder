//
//  TextFieldCellModel.swift
//  Reminder
//
//  Created by mac-001 on 6/1/17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

struct TextFieldCellModel: ReuseID {
  
  var text: String
  var label: String
  var tag: Int
  var action: (String) -> ()
  
  var reuseID = "cellTextField"
  
  init(name reminder: Reminder, action: @escaping (String) -> ()) {
    label = "Name"
    tag = 0
    text = reminder.name
    self.action = action
  }
  
  init(description reminder: Reminder, action: @escaping (String) -> ()) {
    label = "Description"
    tag = 1
    text = reminder.descript
    self.action = action
  }
  
  init(time reminder: Reminder, action: @escaping (String) -> ()) {
    label = "Time"
    tag = 2
    text = MyDateformatter.dateFormatter.string(from: reminder.time)
    self.action = action
  }
}
