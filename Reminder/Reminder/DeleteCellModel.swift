//
//  DeleteCellModel.swift
//  Reminder
//
//  Created by mac-001 on 6/1/17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

struct DeleteCellModel: ReuseID {
  
  var reuseID = "Delete"
  var action: () -> ()
  
  
  init(delete reminder: Reminder, action: @escaping () -> ()) {
    self.action = action
  }
}
