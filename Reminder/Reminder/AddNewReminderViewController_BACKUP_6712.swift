//
//  AddNewReminderViewController.swift
//  Reminder
//
//  Created by Admin on 27.04.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit


protocol ReuseID {
  var reuseID: String { get }
}

<<<<<<< HEAD
=======
protocol Configurable {
  func configure(model: Any, selfDelegate: Any)
}
>>>>>>> parent of 0d17067... version2

class AddNewReminderViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var addButton: UIBarButtonItem!
  
  var reuseID: String = ""
  var model: [CellViewModelForAddAndEdit] = []

  var reminders = Reminder.getReminders()
  
  
  
  @IBAction func cancelToMapViewController(segue:UIStoryboardSegue) {
  }
  
  @IBAction func cancelToImageViewController(segue:UIStoryboardSegue) {
  }
  
  @IBAction func addButton(_ sender: UIBarButtonItem) {
    //    guard let imageForCell = imageForCell, let mapForCell = mapForCell, let nameForCell = nameForCell, let descriptForCell = descriptForCell else { return }
    
    tableView.endEditing(true)
    let nameForImage = MyDateformatter.dateFormatter.string(from: Date() )
    let nameForScreenshot = nameForImage + "2"
    
    if let pngImageData = UIImagePNGRepresentation(model[3].image!) {
      try? pngImageData.write(to: Reminder.getImageUrl(name: nameForImage), options: .atomic)
    }
    if let pngImageData = UIImagePNGRepresentation(model[4].screenshot!) {
      try? pngImageData.write(to: Reminder.getImageUrl(name: nameForScreenshot), options: .atomic)
    }
    
    let reminderForAdd = Reminder(name: "", descript: "", isDone: false, time: Date(), identity: 1, image: nameForImage, screenshot: nameForScreenshot)
    reminderForAdd.name = model[0].name!
    reminderForAdd.descript = model[1].descript!
    if let isDownSwitch = model[5].isDone {
      reminderForAdd.isDone = isDownSwitch
    } else {
      reminderForAdd.isDone = false
    }
    
    if let timeForCell = model[2].time {
      if let time = MyDateformatter.dateFormatter.date(from: timeForCell) {
        reminderForAdd.time = time
      }
    }
    
    if let indexEditing = MyVariables.indexForEditing {
      try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[indexEditing].image).path)
      try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[indexEditing].screenshot).path)
      reminders[indexEditing] = reminderForAdd
    } else {
      reminders.append(reminderForAdd)
    }
    Reminder.setReminders(reminders: reminders)
    performSegue(withIdentifier: "Exit", sender: self)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "Exit" {
    }
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    
    makeModel()
    
    if MyVariables.indexForEditing != nil {
      addButton.title = "Edit"
    }
    
    tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "cellTextField")
    tableView.register(UINib(nibName: "DeleteTableViewCell", bundle: nil), forCellReuseIdentifier: "Delete")
    tableView.register(UINib(nibName: "IsDoneTableViewCell", bundle: nil), forCellReuseIdentifier: "IsDone")
    tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "Image")
    
    tableView.estimatedRowHeight = 200
    tableView.rowHeight = UITableViewAutomaticDimension
    
    NotificationCenter.default.addObserver(self, selector: #selector(AddNewReminderViewController.newMapNotification), name: NSNotification.Name(rawValue: "New Map"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(AddNewReminderViewController.newImageNotification), name: NSNotification.Name(rawValue: "New Image"), object: nil)
  }
  
  func makeModel() {
    
    var cell1 = CellViewModelForAddAndEdit(reuse: "cellTextField")
    var cell2 = CellViewModelForAddAndEdit(reuse: "cellTextField")
    var cell3 = CellViewModelForAddAndEdit(reuse: "cellTextField")
    var cell4 = CellViewModelForAddAndEdit(reuse: "Image")
    var cell5 = CellViewModelForAddAndEdit(reuse: "Image")
    var cell6 = CellViewModelForAddAndEdit(reuse: "IsDone")
    let cell7 = CellViewModelForAddAndEdit(reuse: "Delete")
    
    if MyVariables.indexForEditing != nil {
      
      let reminder = reminders[MyVariables.indexForEditing!]
      
      cell1.name = reminder.name
      cell2.descript = reminder.descript
      cell3.time = MyDateformatter.dateFormatter.string(from: reminder.time)
      cell4.image = UIImage(contentsOfFile: Reminder.getImageUrl(name: reminder.image).path)!
      cell5.screenshot = UIImage(contentsOfFile: Reminder.getImageUrl(name: reminder.screenshot).path)!
      cell6.isDone = reminder.isDone
    } else {
      cell1.name = ""
      cell2.descript = ""
      cell3.time = MyDateformatter.dateFormatter.string(from: Date())
      cell4.image = UIImage(named: "photo")
      cell5.screenshot = UIImage(named: "photo")
      cell6.isDone = false
    }
    
    model.append(cell1)
    model.append(cell2)
    model.append(cell3)
    model.append(cell4)
    model.append(cell5)
    model.append(cell6)
    model.append(cell7)
  }
  
  func newImageNotification (notificacion: Notification) {
    guard let userInfo = notificacion.userInfo else {return}
    guard let info = userInfo["newImage"] as? UIImage else {return}
    model[3].image = info
    tableView.reloadData()
  }
  
  func newMapNotification (notificacion: Notification) {
    guard let userInfo = notificacion.userInfo else {return}
    guard let info = userInfo["newMap"] as? UIImage else {return}
    model[4].screenshot = info
    tableView.reloadData()
  }
}

// MARK: Table View Data Source
extension AddNewReminderViewController: UITableViewDataSource, UITextViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if MyVariables.indexForEditing != nil {
      return 7
    } else {
      return 5
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
<<<<<<< HEAD
    let data = model[indexPath.row]
=======
    let cell = tableView.dequeueReusableCell(withIdentifier: models[indexPath.row].reuseID, for: indexPath) as! Configurable
    cell.configure(model: models[indexPath.row], selfDelegate: self)
>>>>>>> parent of 0d17067... version2
    
    switch indexPath.row {
    case 0:
      let cell = tableView.dequeueReusableCell(withIdentifier: data.reuseID, for: indexPath) as! TextFieldTableViewCell
      cell.nameTextField.delegate = self
      cell.nameLabel.text = "Name"
      cell.nameTextField.placeholder = "Name"
      cell.nameTextField.text = data.name
      return cell
    case 1:
      let cell = tableView.dequeueReusableCell(withIdentifier: data.reuseID, for: indexPath) as! TextFieldTableViewCell
      cell.nameTextField.delegate = self
      cell.nameLabel.text = "Description"
      cell.nameTextField.placeholder = "Description"
      cell.nameTextField.text = data.descript
      return cell
    case 2:
      let cell = tableView.dequeueReusableCell(withIdentifier: data.reuseID, for: indexPath) as! TextFieldTableViewCell
      cell.configureCellForDatePicker()
      cell.nameTextField.delegate = self
      cell.nameLabel.text = "Time"
      cell.nameTextField.placeholder = "Time"
      cell.nameTextField.text = data.time
      return cell
    case 3:
      let cell = tableView.dequeueReusableCell(withIdentifier: data.reuseID, for: indexPath) as! ImageTableViewCell
      cell.imageViewForImage.image = data.image
      return cell
    case 4:
      let cell = tableView.dequeueReusableCell(withIdentifier: data.reuseID, for: indexPath) as! ImageTableViewCell
      cell.imageViewForImage.image = data.screenshot
      return cell
    case 5:
      let cell = tableView.dequeueReusableCell(withIdentifier: data.reuseID, for: indexPath) as! IsDoneTableViewCell
      
      cell.isDoneSwitch.isOn = data.isDone!
      if cell.isDoneSwitch.isOn {
        cell.isDoneLabel.text = "Done"
        cell.isDoneLabel.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
      } else {
        cell.isDoneLabel.text = "Not done"
        cell.isDoneLabel.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
      }
      cell.delegate = self
      return cell
      
    default:
      let cell = tableView.dequeueReusableCell(withIdentifier: "Delete", for: indexPath) as! DeleteTableViewCell
      cell.delegate = self
      return cell
    }
  }
}

extension AddNewReminderViewController: UITextFieldDelegate {
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    if textField.placeholder == "Name" {
      model[0].name = textField.text
    } else if textField.placeholder == "Description" {
      model[1].descript = textField.text
    } else {
      model[2].time = textField.text
    }
  }
}

// MARK: Table View Delegate
extension AddNewReminderViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if indexPath.row == 3 {
      performSegue(withIdentifier: "imageSegue", sender: self)
    }
    if indexPath.row == 4 {
      performSegue(withIdentifier: "mapSegue", sender: self)
    }
  }
}

extension AddNewReminderViewController: IsDoneDelegate {
  func isDoneSwitch(_ switcher: Bool) {
    model[5].isDone = switcher
  }
}

extension AddNewReminderViewController: DeleteButtonDelegate {
  func deleteButtonPress() {
    try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[MyVariables.indexForEditing!].image).path)
    try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[MyVariables.indexForEditing!].screenshot).path)
    reminders.remove(at: MyVariables.indexForEditing!)
    Reminder.setReminders(reminders: reminders)
    performSegue(withIdentifier: "Exit", sender: self)
    print("delete")
  }
}









