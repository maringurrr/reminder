//
//  MapViewController.swift
//  Reminder
//
//  Created by Admin on 08.05.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
  
  let locationManager = CLLocationManager()
  
  var image: UIImage?
  var nameImage: String?
  var action: ((UIImage) -> ())?
  
  @IBOutlet weak var mapView: MKMapView!
  
  @IBAction func saveMapButton(_ sender: UIBarButtonItem) {
    
    snapShotter(snapShotForFacebook: false)
    performSegue(withIdentifier: "closeMapSegue", sender: self)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(false)
    
    if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
      locationManager.requestWhenInUseAuthorization()
    }
  }

  func snapShotter (snapShotForFacebook: Bool) {
    
    let mapSnapshotOptions = MKMapSnapshotOptions()
    mapView.showsUserLocation = false
    mapSnapshotOptions.region = mapView.region
    
    mapSnapshotOptions.scale = UIScreen.main.scale
    mapSnapshotOptions.size = CGSize(width: 200, height: 200)
    
    let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
    
    snapShotter.start { (snapShot, error)  in
      guard let snapShot = snapShot else {
        print("Snapshot error: \(String(describing: error))")
        return
      }
      
      let pin = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
      let image = snapShot.image
      UIGraphicsBeginImageContext(image.size)
      image.draw(at: CGPoint.zero)
      
      let visibleRect = CGRect(origin: CGPoint.zero, size: image.size)
      for annotation in self.mapView.annotations {
        var point = snapShot.point(for: annotation.coordinate)
        if visibleRect.contains(point) {
          point.x = point.x + pin.centerOffset.x - (pin.bounds.size.width / 2)
          point.y = point.y + pin.centerOffset.y - (pin.bounds.size.height / 2)
          pin.image?.draw(at: point)
        }
      }
      let chosenImage = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsBeginImageContext(CGSize(width: 80, height: 80))
      chosenImage?.draw(in: CGRect(x: 0, y: 0, width: 80, height: 80))
      let newImage = UIGraphicsGetImageFromCurrentImageContext()
      
      UIGraphicsEndImageContext()
      
      if let newImage = newImage {
        self.action!(newImage)
      }

    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    mapView.delegate = self
    mapView.showsUserLocation = true
    
    let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MapViewController.handleTap(gestureReconizer:)))
    gestureRecognizer.delegate = self
    mapView.addGestureRecognizer(gestureRecognizer)
    
  }
}

extension MapViewController: MKMapViewDelegate {
  
  func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 10000, 10000)
    mapView.setRegion(coordinateRegion, animated: false)
    let dropPin = MKPointAnnotation()
    dropPin.coordinate = userLocation.coordinate
    dropPin.title = "MINSK"
    mapView.addAnnotation(dropPin)
  }
}

extension MapViewController: UIGestureRecognizerDelegate {
  
  func handleTap(gestureReconizer: UILongPressGestureRecognizer) {
    mapView.removeAnnotations(mapView.annotations)
    let location = gestureReconizer.location(in: mapView)
    let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
    mapView.setCenter(coordinate, animated: true)
    let annotation = MKPointAnnotation()
    annotation.coordinate = coordinate
    mapView.addAnnotation(annotation)
  }
}
