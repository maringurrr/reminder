//
//  IsDoneTableViewCell.swift
//  Reminder
//
//  Created by Admin on 28.04.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

class IsDoneTableViewCell: UITableViewCell, Configurable {
  
  @IBOutlet weak var isDoneSwitch: UISwitch!
  @IBOutlet weak var isDoneLabel: UILabel!
  
  var isSwitch: Bool = false
  var action: ((Bool) -> ())!
  
  func configure (model: Any) {
    let model = model as! IsDoneCellModel
    isDoneSwitch.isOn = model.isDone!
    action = model.action
    
    if isDoneSwitch.isOn {
      isDoneLabel.text = "Done"
      isDoneLabel.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
    } else {
      isDoneLabel.text = "Not done"
      isDoneLabel.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
    }
  }
  
  @IBAction func isDoneSwitchAction(_ sender: AnyObject) {
    if isDoneSwitch.isOn {
      isSwitch = true
      isDoneLabel.text = "Done"
      isDoneLabel.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
      action(true)
    } else {
      isSwitch = false
      isDoneLabel.text = "Not done"
      isDoneLabel.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
      action(false)
    }
  }
}
