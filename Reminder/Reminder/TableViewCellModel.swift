//
//  TableViewCellModel.swift
//  Reminder
//
//  Created by mac-001 on 5/29/17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

class TableViewCellModel {
  var name: String
  var descript: String
  var isDone: Bool
  var time: String
  var identity: Int
  var image: UIImage
  var screenshot: UIImage
  
  init(reminder: Reminder) {
    self.name = reminder.name
    self.descript = reminder.descript
    self.isDone = reminder.isDone
    self.time = MyDateformatter.dateFormatter.string(from: reminder.time)
    self.identity = reminder.identity
    self.image = UIImage(contentsOfFile: Reminder.getImageUrl(name: reminder.image).path)!
    self.screenshot = UIImage(contentsOfFile: Reminder.getImageUrl(name: reminder.screenshot).path)!
  }
}
