//
//  ImageCellModel.swift
//  Reminder
//
//  Created by mac-001 on 6/1/17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

struct ImageCellModel: ReuseID {
  
  var image: UIImage?
  var reuseID = "Image"
  var identForSegue = ""
  var action: (UIImage) -> ()
  
  init(image reminder: Reminder, action: @escaping (UIImage) -> ()) {
    if reminder.image == "" {
      image = UIImage(named: "photo")
    } else {
      image = UIImage(contentsOfFile: Reminder.getImageUrl(name: reminder.image).path)
    }
    identForSegue = "Image"
    self.action = action
  }
  
  init(map reminder: Reminder, action: @escaping (UIImage) -> ()) {
    if reminder.screenshot == "" {
      image = UIImage(named: "photo")
    } else {
      image = UIImage(contentsOfFile: Reminder.getImageUrl(name: reminder.screenshot).path)
    }
    self.action = action
    identForSegue = "Map"
  }
}
