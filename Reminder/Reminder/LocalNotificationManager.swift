//
//  LocalNotificationManager.swift
//  Reminder
//
//  Created by Admin on 18.04.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import Foundation
import UserNotifications

class LocalNotificationManager {
  
  static let sharedInstance = LocalNotificationManager()
  
  func doNotification() {
    NotificationCenter.default.addObserver(self, selector: #selector(LocalNotificationManager.makeNotification), name: NSNotification.Name(rawValue: "Was added a new reminder"), object: nil)
  }
  
  @objc func makeNotification() {
    let reminders = Reminder.getReminders()
    UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    
    reminders.forEach { (reminder) in
      let strDate = Date()
      let timeInterval = -strDate.timeIntervalSince(reminder.time)
      
      if timeInterval > 0 {
        let content = UNMutableNotificationContent()
        content.title = reminder.name
        content.body = "Time is down"
        
        let trigger2 = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
        let request = UNNotificationRequest(identifier: "\(reminder.identity)", content: content, trigger: trigger2)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
      }
    }
  }
}
