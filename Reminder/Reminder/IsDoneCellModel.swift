//
//  IsDoneCellModel.swift
//  Reminder
//
//  Created by mac-001 on 6/1/17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

struct IsDoneCellModel: ReuseID {
  
  var isDone: Bool?
  var action: (Bool) -> ()
  var reuseID = "IsDone"
  
  init(isDone reminder: Reminder, action: @escaping (Bool) -> ()) {
    isDone = reminder.isDone
    self.action = action
  }
}
