//
//  MyDateformatter.swift
//  Reminder
//
//  Created by Admin on 06.12.16.
//  Copyright © 2016 Mikalai. All rights reserved.
//

import Foundation

struct MyDateformatter {
  
  static var dateFormatter: DateFormatter = {
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.dateStyle = .long
    dateFormatter.timeStyle = .short
    return dateFormatter
  }()
}

