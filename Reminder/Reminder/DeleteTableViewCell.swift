//
//  DeleteTableViewCell.swift
//  Reminder
//
//  Created by Admin on 28.04.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit


class DeleteTableViewCell: UITableViewCell, Configurable {

  @IBOutlet private weak var deleteButton: UIButton!
  var action: (() -> ())!
  
  @IBAction func deleteButton(_ sender: UIButton) {
    action()
  }
  func configure(model: Any) {
    let model = model as! DeleteCellModel
    action = model.action
  }
}
