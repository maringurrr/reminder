//
//  ImageTableViewCell.swift
//  Reminder
//
//  Created by Admin on 28.04.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell, Configurable {

  @IBOutlet weak var imageViewForImage: UIImageView!

  func configure (model: Any) {
    let model = model as! ImageCellModel
    imageViewForImage.image = model.image
  }
}
