//
//  AddNewReminderViewController.swift
//  Reminder
//
//  Created by Admin on 27.04.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

protocol ReuseID {
  var reuseID: String { get }
}

protocol Configurable {
  func configure(model: Any)
}

class AddNewReminderViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var addButton: UIBarButtonItem!
  
  var arrayImageForDelete: [String] = []
  var arrayScreenForDelete: [String] = []
  
  var identityReminder: Int? = nil
  var reminder: Reminder!
  
  var reuseID: String = ""
  var models: [ReuseID] = []
  var reminders = Reminder.getReminders()
  
  @IBAction func cancelToMapViewController(segue:UIStoryboardSegue) {
  }
  
  @IBAction func cancelToImageViewController(segue:UIStoryboardSegue) {
  }
  
  @IBAction func addButton(_ sender: UIBarButtonItem) {
    
    tableView.endEditing(true)
    
    if let indexEditing = identityReminder {
      
      arrayImageForDelete.insert(reminders[indexEditing].image, at: 0)
      arrayScreenForDelete.insert(reminders[indexEditing].screenshot, at: 0)
      
      reminders[indexEditing] = reminder
    } else {
      
      if arrayImageForDelete.isEmpty {
        let nameForImage = UUID().uuidString
        if let pngImageData = UIImagePNGRepresentation(UIImage(named: "photo")!) {
          try? pngImageData.write(to: Reminder.getImageUrl(name: nameForImage), options: .atomic)
        }
        reminder.image = nameForImage
      }
      if arrayScreenForDelete.isEmpty {
        let nameForScreen = UUID().uuidString
        if let pngImageData = UIImagePNGRepresentation(UIImage(named: "photo")!) {
          try? pngImageData.write(to: Reminder.getImageUrl(name: nameForScreen), options: .atomic)
        }
        reminder.screenshot = nameForScreen
      }
      
      reminders.append(reminder)
      
      arrayImageForDelete.insert("", at: 0)
      arrayScreenForDelete.insert("", at: 0)
    }
    arrayImageForDelete.removeLast()
    arrayScreenForDelete.removeLast()
    
    Reminder.setReminders(reminders: reminders)
    performSegue(withIdentifier: "Exit", sender: self)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if identityReminder == nil {
      reminder = Reminder(name: "", descript: "", isDone: false, time: Date(), identity: 1, image: "", screenshot: "")
    } else {
      reminder = Reminder.getReminders()[identityReminder!]
      addButton.title = "Edit"
    }
    
    makeModel()
    
    tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "cellTextField")
    tableView.register(UINib(nibName: "DeleteTableViewCell", bundle: nil), forCellReuseIdentifier: "Delete")
    tableView.register(UINib(nibName: "IsDoneTableViewCell", bundle: nil), forCellReuseIdentifier: "IsDone")
    tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "Image")
    
    tableView.estimatedRowHeight = 100
    tableView.rowHeight = UITableViewAutomaticDimension
  }
  
  deinit {
    arrayImageForDelete.forEach { (name) in
      try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: name).path)
    }
    arrayScreenForDelete.forEach { (name) in
      try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: name).path)
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "imageSegue" {
      if let viewController = segue.destination as? ImageViewController {
        models.forEach({ (model) in
          if model.reuseID == "Image" {
            let modell = model as! ImageCellModel
            if modell.identForSegue == "Image" {
              viewController.action = modell.action
              viewController.nameImage = reminder.image
            }
          }
        })
      }
    }
    
    if segue.identifier == "mapSegue" {
      if let viewController = segue.destination as? MapViewController {
        models.forEach({ (model) in
          if model.reuseID == "Image" {
            let modell = model as! ImageCellModel
            if modell.identForSegue == "Map" {
              viewController.action = modell.action
            }
          }
        })
      }
    }
  }
  
  private func makeModel() {
    models.removeAll()
    models.append(TextFieldCellModel(name: reminder) { [weak self] (name) in
      self?.reminder.name = name
      self?.makeModel()
    })
    models.append(TextFieldCellModel(description: reminder) { [weak self] (descript) in
      self?.reminder.descript = descript
      self?.makeModel()
    })
    models.append(TextFieldCellModel(time: reminder) { [weak self] (time) in
      if let time = MyDateformatter.dateFormatter.date(from: time) {
        self?.reminder.time = time
      } else {
        self?.reminder.time = Date()
      }
      self?.makeModel()
    })
    models.append(ImageCellModel(image: reminder) { [weak self] (name) in
      
      let nameForImage = UUID().uuidString
      if let pngImageData = UIImagePNGRepresentation(name) {
        try? pngImageData.write(to: Reminder.getImageUrl(name: nameForImage), options: .atomic)
      }
      
      self?.arrayImageForDelete.append(nameForImage)
      self?.reminder.image = nameForImage
      
      self?.makeModel()
    })
    models.append(ImageCellModel(map: reminder) { [weak self] (name) in
      
      let nameForScreen = UUID().uuidString
      if let pngImageData = UIImagePNGRepresentation(name) {
        try? pngImageData.write(to: Reminder.getImageUrl(name: nameForScreen), options: .atomic)
      }
      
      self?.arrayScreenForDelete.append(nameForScreen)
      self?.reminder.screenshot = nameForScreen
      
      self?.makeModel()
      
    })
    models.append(IsDoneCellModel(isDone: reminder) { [weak self] (isDone) in
      self?.reminder.isDone = isDone
      self?.makeModel()
    })
    models.append(DeleteCellModel(delete: reminder) { [weak self] () in
      self?.deleteButtonPress()
    })
    self.tableView.reloadData()
  }
  
  func deleteButtonPress() {
    try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[identityReminder!].image).path)
    try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[identityReminder!].screenshot).path)
    reminders.remove(at: identityReminder!)
    Reminder.setReminders(reminders: reminders)
    performSegue(withIdentifier: "Exit", sender: self)
  }
}

// MARK: Table View Data Source
extension AddNewReminderViewController: UITableViewDataSource, UITextViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if identityReminder == nil {
      return models.count - 2
    } else {
      return models.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: models[indexPath.row].reuseID, for: indexPath)
    (cell as? Configurable)?.configure(model: models[indexPath.row])
    return cell
  }
}

// MARK: Table View Delegate
extension AddNewReminderViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
    if models[indexPath.row].reuseID == "Image" {
      let model = models[indexPath.row] as! ImageCellModel
      if model.identForSegue == "Image" {
        performSegue(withIdentifier: "imageSegue", sender: self)
      }
      if model.identForSegue == "Map" {
        performSegue(withIdentifier: "mapSegue", sender: self)
      }
    }
  }
}


