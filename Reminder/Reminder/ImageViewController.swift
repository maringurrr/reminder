//
//  ImageViewController.swift
//  Reminder
//
//  Created by Admin on 09.05.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
  
  
  @IBOutlet weak var imageView: UIImageView!
  
  var nameImage: String?
  var action: ((UIImage) -> ())?
  
  @IBAction func SaveImageButton(_ sender: UIBarButtonItem) {
    performSegue(withIdentifier: "closeImageSegue", sender: self)
  }
  @IBAction func changeImageButton(_ sender: UIButton) {
    imagePikerPresent()
  }
  
  func chooseImagePickerAction(sourse: UIImagePickerControllerSourceType) {
    if UIImagePickerController.isSourceTypeAvailable(sourse) {
      let imagePicker = UIImagePickerController()
      imagePicker.delegate = self
      imagePicker.allowsEditing = true
      imagePicker.sourceType = sourse
      self.present(imagePicker, animated: true, completion: nil)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    imageView.image = UIImage(contentsOfFile: Reminder.getImageUrl(name: nameImage!).path)
    
    imagePikerPresent()
  }
  
  func imagePikerPresent() {
    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
      let imagePicker = UIImagePickerController()
      imagePicker.delegate = self
      imagePicker.allowsEditing = true
      imagePicker.sourceType = .photoLibrary
      self.present(imagePicker, animated: true, completion: nil)
    }
  }
}

extension ImageViewController: UIImagePickerControllerDelegate {
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
    let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
    let widthRatio = imageView.frame.size.width / chosenImage.size.width
    let heightRatio = 119 / chosenImage.size.height
    var newSize: CGSize
    if(widthRatio > heightRatio) {
      newSize = CGSize(width: chosenImage.size.width * heightRatio, height: chosenImage.size.height * heightRatio)
    } else {
      newSize = CGSize(width: chosenImage.size.width * widthRatio,  height: chosenImage.size.height * widthRatio)
    }
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    chosenImage.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    imageView.image = newImage
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true

    if let newImage = imageView.image {
      action!(newImage)
    }
    dismiss(animated: false, completion: {self.performSegue(withIdentifier: "closeImageSegue", sender: self)})
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    dismiss(animated: true, completion: nil)
  }
}

extension ImageViewController: UINavigationControllerDelegate {
}
