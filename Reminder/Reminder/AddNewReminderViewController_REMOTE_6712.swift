//
//  AddNewReminderViewController.swift
//  Reminder
//
//  Created by Admin on 27.04.17.
//  Copyright © 2017 Mikalai. All rights reserved.
//

import UIKit

protocol ReuseID {
  var reuseID: String { get }
}

protocol Configurable {
  func configure(model: Any, selfDelegate: Any)
}

class AddNewReminderViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var addButton: UIBarButtonItem!
  
  var reuseID: String = ""
  var models: [ReuseID] = []
  var reminders = Reminder.getReminders()

  @IBAction func cancelToMapViewController(segue:UIStoryboardSegue) {
  }
  
  @IBAction func cancelToImageViewController(segue:UIStoryboardSegue) {
  }
  
  @IBAction func addButton(_ sender: UIBarButtonItem) {
    
    tableView.endEditing(true)
    
    let model0 = models[0] as! TextFieldCellModel
    let model1 = models[1] as! TextFieldCellModel
    let model2 = models[2] as! TextFieldCellModel
    let model3 = models[3] as! ImageCellModel
    let model4 = models[4] as! ImageCellModel
    let model5 = models[5] as! IsDoneCellModel

    let dateformatterForName = DateFormatter()
    dateformatterForName.timeStyle = .full
    let nameForImage = dateformatterForName.string(from: Date() )
    let nameForScreenshot = nameForImage + "2"
    
    if let pngImageData = UIImagePNGRepresentation(model3.image!) {
      try? pngImageData.write(to: Reminder.getImageUrl(name: nameForImage), options: .atomic)
    }
    if let pngImageData = UIImagePNGRepresentation(model4.image!) {
      try? pngImageData.write(to: Reminder.getImageUrl(name: nameForScreenshot), options: .atomic)
    }
    let reminderForAdd = Reminder(name: "", descript: "", isDone: false, time: Date(), identity: 1, image: nameForImage, screenshot: nameForScreenshot)
    
    reminderForAdd.name = model0.text!
    reminderForAdd.descript = model1.text!
    if let isDownSwitch = model5.isDone {
      reminderForAdd.isDone = isDownSwitch
    } else {
      reminderForAdd.isDone = false
    }
    
    if let timeForCell = model2.text {
      if let time = MyDateformatter.dateFormatter.date(from: timeForCell) {
        reminderForAdd.time = time
      }
    }
    
    if let indexEditing = MyVariables.indexForEditing {
      try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[indexEditing].image).path)
      try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[indexEditing].screenshot).path)
      reminders[indexEditing] = reminderForAdd
    } else {
      reminders.append(reminderForAdd)
    }
    Reminder.setReminders(reminders: reminders)
    performSegue(withIdentifier: "Exit", sender: self)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    makeModel()
    
    if MyVariables.indexForEditing != nil {
      addButton.title = "Edit"
    }
    
    tableView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "cellTextField")
    tableView.register(UINib(nibName: "DeleteTableViewCell", bundle: nil), forCellReuseIdentifier: "Delete")
    tableView.register(UINib(nibName: "IsDoneTableViewCell", bundle: nil), forCellReuseIdentifier: "IsDone")
    tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "Image")
    
    tableView.estimatedRowHeight = 100
    tableView.rowHeight = UITableViewAutomaticDimension
    
    NotificationCenter.default.addObserver(self, selector: #selector(AddNewReminderViewController.newMapNotification), name: NSNotification.Name(rawValue: "New Map"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(AddNewReminderViewController.newImageNotification), name: NSNotification.Name(rawValue: "New Image"), object: nil)
  }
  
  private func makeModel() {
    let reminder: Reminder?
    
    if MyVariables.indexForEditing != nil {
      reminder = reminders[MyVariables.indexForEditing!]
    } else {
      reminder = nil
    }
    let cell0 = TextFieldCellModel(name: reminder)
    let cell1 = TextFieldCellModel(description: reminder)
    let cell2 = TextFieldCellModel(time: reminder)
    let cell3 = ImageCellModel(image: reminder)
    let cell4 = ImageCellModel(map: reminder)
    let cell5 = IsDoneCellModel(reminder: reminder)
    let cell6 = DeleteCellModel()
    models.append(cell0)
    models.append(cell1)
    models.append(cell2)
    models.append(cell3)
    models.append(cell4)
    models.append(cell5)
    models.append(cell6)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "Exit" {
    }
  }

  fileprivate func changeModel (number: Int, value: Any?) {
    
    switch number {
    case 0, 1, 2:
      var model = models[number] as! TextFieldCellModel
      model.text = value as? String
      models[number] = model
    case 3, 4:
      var model = models[number] as! ImageCellModel
      model.image = value as? UIImage
      models[number] = model
    default:
      var model = models[number] as! IsDoneCellModel
      model.isDone = value as? Bool
      models[number] = model
    }
  }
  
  @objc private func newImageNotification (notificacion: Notification) {
    guard let userInfo = notificacion.userInfo else {return}
    guard let info = userInfo["newImage"] as? UIImage else {return}
    
    changeModel(number: 3, value: info)
    tableView.reloadData()
  }
  
  @objc private func newMapNotification (notificacion: Notification) {
    guard let userInfo = notificacion.userInfo else {return}
    guard let info = userInfo["newMap"] as? UIImage else {return}
    
    changeModel(number: 4, value: info)
    tableView.reloadData()
  }
}

// MARK: Table View Data Source
extension AddNewReminderViewController: UITableViewDataSource, UITextViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if MyVariables.indexForEditing != nil {
      return 7
    } else {
      return 5
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: models[indexPath.row].reuseID, for: indexPath) as! Configurable
    cell.configure(model: models[indexPath.row], selfDelegate: self)
    
    return cell as! UITableViewCell
  }
}

// MARK: Table View Delegate
extension AddNewReminderViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if indexPath.row == 3 {
      performSegue(withIdentifier: "imageSegue", sender: self)
    }
    if indexPath.row == 4 {
      performSegue(withIdentifier: "mapSegue", sender: self)
    }
  }
}

extension AddNewReminderViewController: UITextFieldDelegate {
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    
    if textField.placeholder == "Name" {
      print("name")
      changeModel(number: 0, value: textField.text)
    } else if textField.placeholder == "Description" {
      print("descript")
      changeModel(number: 1, value: textField.text)
    } else {
      print("time")
      changeModel(number: 2, value: textField.text)
    }
  }
}

extension AddNewReminderViewController: IsDoneDelegate {
  func isDoneSwitch(_ switcher: Bool) {
    print("isDone")
    changeModel(number: 5, value: switcher)
  }
}

extension AddNewReminderViewController: DeleteButtonDelegate {
  func deleteButtonPress() {
    try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[MyVariables.indexForEditing!].image).path)
    try? FileManager.default.removeItem(atPath: Reminder.getImageUrl(name: reminders[MyVariables.indexForEditing!].screenshot).path)
    reminders.remove(at: MyVariables.indexForEditing!)
    Reminder.setReminders(reminders: reminders)
    performSegue(withIdentifier: "Exit", sender: self)
    print("delete")
  }
}

