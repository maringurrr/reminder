//
//  MainScreenViewController.swift
//  Reminder
//
//  Created by Admin on 24.11.16.
//  Copyright © 2016 Mikalai. All rights reserved.
//

import UIKit
import UserNotifications

class MainScreenViewController: UIViewController {
  
  @IBOutlet private weak var newOldVC: UIBarButtonItem!
  @IBOutlet private weak var tableview1: UITableView!
  
  fileprivate var sectionsNames = [String]()
  fileprivate var sectionsDataModels = [String: [TableViewCellModel]]()
  
  fileprivate let segueForEditingEvents = "newEditingSegue"
  
  @IBAction func cancelToPlayersViewController(segue:UIStoryboardSegue) {
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    makeModels()
    
    LocalNotificationManager.sharedInstance.doNotification()
    updeaterTableForTimer()
    
    let center = UNUserNotificationCenter.current()
    center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in }
    
    NotificationCenter.default.addObserver(self, selector: #selector(MainScreenViewController.makeModels), name: NSNotification.Name(rawValue: "Was added a new reminder"), object: nil)
  }
  
//  override func viewWillAppear(_ animated: Bool) {
//    super.viewWillAppear(true)
//    MyVariables.indexForEditing = nil
//  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == segueForEditingEvents {
      if let viewController = segue.destination as? AddNewReminderViewController {
        let indexPath = tableview1.indexPathForSelectedRow!
        viewController.identityReminder = model(forIndexPath: indexPath).identity
        tableview1.deselectRow(at: indexPath, animated: true)
      }
    }
    if segue.identifier == "addSegue" {
    }
  }
  
  @objc private func updeaterTableForTimer() {
    let calendar = Calendar.current
    let components = calendar.dateComponents([.second, .nanosecond], from: Date())
    let doubleSeconds = Double(components.second!)
    let doubleMilliseconds = Double(components.nanosecond! / 1_000_000)
    let timeInterval = 61 - (doubleSeconds + (doubleMilliseconds / 1000))
    
    _ = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(MainScreenViewController.updeaterTableForTimer), userInfo: nil, repeats: false)
    tableview1.reloadData()
  }
  
  @objc private func makeModels() {
    
    var arrayForHelpMakeModel = [TableViewCellModel]()
    sectionsNames.removeAll()
    sectionsDataModels.removeAll()
    
    var numberOfBadge = 0
    var strDate: String = ""
    let dateFormatterForSubs = DateFormatter()
    dateFormatterForSubs.dateFormat = "dd-MM-yyyy"
    
    let reminders = Reminder.getReminders()
    reminders.forEach { (reminder) in
      
      let newModel = TableViewCellModel(reminder: reminder)
      let str = MyDateformatter.dateFormatter.date(from: newModel.time)
      strDate = dateFormatterForSubs.string(from: str!)
      
      if sectionsNames.last == strDate {
        arrayForHelpMakeModel.append(newModel)
      } else {
        sectionsNames.append(strDate)
        arrayForHelpMakeModel.removeAll()
        arrayForHelpMakeModel.append(newModel)
      }
      sectionsDataModels[strDate] = arrayForHelpMakeModel
      
      if reminder.isDone == false {
        numberOfBadge += 1
      }
    }
    UIApplication.shared.applicationIconBadgeNumber = numberOfBadge
    tableview1.reloadData()
  }
  
  fileprivate func model(forIndexPath indexPath: IndexPath) -> TableViewCellModel {
    let name = sectionsNames[indexPath.section]
    let array = sectionsDataModels[name]!
    return array[indexPath.row]
  }
}

// MARK: Table View Data Source
extension MainScreenViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let name = sectionsNames[section]
    let array = sectionsDataModels[name]!
    return array.count
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return sectionsNames.count
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return sectionsNames[section]
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
    
    cell.configure(model: model(forIndexPath: indexPath))
    return cell
  }
}

// MARK: Table View Delegate
extension MainScreenViewController: UITableViewDelegate {
  
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    tableView.deselectRow(at: indexPath, animated: true)
//    MyVariables.indexForEditing = model(forIndexPath: indexPath).identity
    
    performSegue(withIdentifier: segueForEditingEvents, sender: self)
  }
}
